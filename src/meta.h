#ifndef META_H
#define META_H

#include "mastor.h"
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void print_meta(FILE *file, int marker, struct ANNOTATION anno, struct MARKER_STATS stat);

#endif
