#ifndef TDIST_H
#define TDIST_H

double t_q2p(const int df, const double t);
double beta_inc(const double a,const double x);
double beta_cont_frac(const double a, const double b,const double x);
double lngamma_lanczos(double x);

/* coefficients for gamma=7, kmax=8  Lanczos method */
static double lanczos_7_c[9] = {
  0.99999999999980993227684700473478,
  676.520368121885098567009190444019,
 -1259.13921672240287047156078755283,
  771.3234287776530788486528258894,
 -176.61502916214059906584551354,
  12.507343278686904814458936853,
 -0.13857109526572011689554707,
  9.984369578019570859563e-6,
  1.50563273514931155834e-7
};

#endif
