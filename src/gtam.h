#ifndef GTAM_H
#define GTAM_H

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include "nrutil.h"
#include "mastor.h"
#include "tdist.h"
#include "cholesky.h"
#include "svdcomp.h"

void initialize_gtam(struct GTAM *gtam, struct DATA_STRUCT data_struct);
void fill_gtam(struct GTAM *gtam, struct FAMILY family, struct DATA_STRUCT data_struct, struct MLE_RESULTS mle_res);
void calc_gtam(struct GTAM *gtam, struct DATA_STRUCT data_struct, struct MLE_RESULTS mle_res);

#endif
