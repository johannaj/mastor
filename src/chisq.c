#include <math.h>
#include <assert.h>
#include "chisq.h"

/* Take in quantile x return p-value for Chisq_df_1 of
    Probability(q>=x). Adapted from GNU GSL Gamma functions.
    Because we are only interested in the Chisq_df_1 we only
    need to adjust a small subset of the GNU GSL functions.
    If T~Chisq_df_1 then T/2~Gamma_scale_1/2_shape_1.
    Therefore we can fix a=scale=1/2. GNU GSL already
    fix b=shape=1 (because it is easy to adjust any Gamma
    distributed statistic to Gamma with shape=1) */
double chisq_q2p(const double x) {
    double y = x / 2.0;

    if (x <= 0.0) {
      return 1.0;
    }

    if (y < 0.5) {
      return 1 - gamma_inc_P(y);
    } else {
     return gamma_inc_Q(y);
    }
}

/* assumptions: 0 <= x < 0.5 */
double gamma_inc_P(const double x) {
    assert(x >= 0.0 && "x must be >= 0");
    if (x==0.0) {
        return 0.0;
    }

    /* code below adapted from gamma_inc_P_series(x) */
    const int nmax = 10000;
    const double sqrt_pi = 1.77245385090551;
    const double eps = 2.2204460492503131e-16;
    double D = 2*sqrt(x)*exp(-x)/sqrt_pi;

    /* Approximating the terms of the series using Stirling's
     approximation gives t_n = (x/a)^n * exp(-n(n+1)/(2a)), so the
     convergence condition is n^2 / (2a) + (1-(x/a) + (1/2a)) n >>
     -log(GSL_DBL_EPS) if we want t_n < O(1e-16) t_0. The condition
     below detects cases where the minimum value of n is > 5000 */

    /* Normal case: sum the series */

    double sum  = 1.0;
    double term = 1.0;
    double remainder;
    int n;

    /* Handle lower part of the series where t_n is increasing, |x| > a+n */

    /* Handle upper part of the series where t_n is decreasing, |x| < a+n */

    for (n=1; n<nmax; n++)  {
      term *= x/(n+0.5);
      sum  += term;
      if(fabs(term/sum) < eps) break;
    }

    /*  Estimate remainder of series ~ t_(n+1)/(1-x/(a+n+1)) */
    {
      double tnp1 = (x/(n+0.5)) * term;
      remainder =  tnp1 / (1.0 - x/( n + 1.5));
    }

    if(n == nmax && fabs(remainder/sum) > eps) {
        assert(0 && "gamma_inc_P series failed to converge");
    } else {
        return  D * sum;
    }
}

/* assumption: x >= 0.5 */
double gamma_inc_Q(const double x) {
    if (x >= 1e4) {
        return 0.0;
    }

    const double sqrt_pi = 1.77245385090551;
    const double eps = 2.2204460492503131e-16;
    double D = 2*sqrt(x)*exp(-x)/sqrt_pi;

    const int    nmax  =  5000;
    const double small =  eps*eps*eps;

    double hn = 1.0;           /* convergent */
    double Cn = 1.0 / small;
    double Dn = 1.0;
    int n;

    /* n == 1 has a_1, b_1, b_0 independent of a,x,
        so that has been done by hand                */
    for ( n = 2 ; n < nmax ; n++ )
    {
        double an;
        double delta;

        an = 0.5*(n-1)/x;
        Dn = 1.0 + an * Dn;
        if ( fabs(Dn) < small ) {
            Dn = small;
        }
        Cn = 1.0 + an/Cn;
        if ( fabs(Cn) < small ) {
            Cn = small;
        }
        Dn = 1.0 / Dn;
        delta = Cn * Dn;
        hn *= delta;
        if(fabs(delta-1.0) < eps) break;
    }

    return hn * D * (0.5/x);

}
