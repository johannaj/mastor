#include "plot.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "nrutil.h"


void plot_like (char *filename, double x1, double x2, double inc, double (*func)(double)) {
	double x, fx;
	
    FILE *plotfile;
    plotfile = fopen(filename,"w");
    fprintf(plotfile,"Alpha\tnegLogLike\n");
    fclose(plotfile);
    plotfile = fopen(filename,"a+");    
	x = x1;
	while (x <= x2) {
		fx = (*func)(x);
        fprintf(plotfile, "%lf\t%lf\n", x, fx);
		x = x + inc;
	}
    fclose(plotfile);
}
