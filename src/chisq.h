#ifndef CHISQ_H
#define CHISQ_H

double chisq_q2p(const double x);
double gamma_inc_P(const double x);
double gamma_inc_Q(const double x);

#endif
