#include "gtam.h"

/* intialize gtam and allocate memory */
void initialize_gtam(struct GTAM *gtam, struct DATA_STRUCT data_struct) {

	int i,j;
	int n_cov = data_struct.n_cov;

	gtam->dd_est = 0.0;

	for(i=1; i<=n_cov+1; i++) {
		gtam->d_est[i] = 0.0;
		for(j=1; j<=n_cov+1; j++) {
			gtam->m_est[i][j] = 0.0;
		}
	}
}

/* fill gtam one family at a time, each family adds to gtam */
void fill_gtam(struct GTAM *gtam, struct FAMILY family, struct DATA_STRUCT data_struct, struct MLE_RESULTS mle_res) {

	int i, j, k;
	int n_geno_pheno = family.n_geno_pheno;
	int n_cov = data_struct.n_cov;
	double add_var_est = mle_res.add_var_hat;
	double err_var_est = mle_res.err_var_hat;
	double alpha_est = add_var_est/err_var_est;

	if (n_geno_pheno == 0) {
		return;
	}

	double **u, *lambda;
	u = dmatrix(1,n_geno_pheno,1,n_geno_pheno);
	lambda = dvector(1,n_geno_pheno);

	double **phi;
	phi = dmatrix(1,n_geno_pheno,1,n_geno_pheno);
	for(j=1; j<=n_geno_pheno; j++) {
		for(k = 1; k<=n_geno_pheno; k++) {
			phi[j][k] = family.phi[family.geno_pheno_typed[j][1]][family.geno_pheno_typed[k][1]];
		}
	}

	double **m;
	m = dmatrix(1,n_geno_pheno,1,n_cov+1);
	for(j=1; j<=n_geno_pheno; j++) {
		for(k = 1; k<=n_cov; k++) {
			m[j][k] = family.cov[family.geno_pheno_typed[j][2]][k];
		}
		m[j][n_cov+1] = 2*family.y[j];
	}

	double *trait;
	trait = dvector(1,n_geno_pheno);
	for(j=1; j<=n_geno_pheno; j++){
		trait[j] = family.trait[family.geno_pheno_typed[j][2]];
	}

    svdcomp(phi, n_geno_pheno, n_geno_pheno, lambda, u);

	double **m_u, *d_u;
	m_u = dmatrix(1,n_cov+1,1,n_geno_pheno);
	d_u = dvector(1,n_geno_pheno);

	for(i=1; i<=n_cov+1; i++) {
		for(j=1; j<=n_geno_pheno; j++) {
			m_u[i][j] = 0.0;
			for (k=1; k<= n_geno_pheno; k++) {
				m_u[i][j] = m_u[i][j] + m[k][i]*u[k][j];
			}
		}
	}

	for(j=1; j<=n_geno_pheno; j++) {
		d_u[j] = 0.0;
		for (k=1; k<= n_geno_pheno; k++) {
			d_u[j] = d_u[j] + trait[k]*u[k][j];
		}
	}


	for(i=1; i<=n_cov+1; i++) {
		for(j=1; j<=n_cov+1; j++) {
			for (k=1; k<= n_geno_pheno; k++) {
				gtam->m_est[i][j] = gtam->m_est[i][j] + m_u[i][k]*m_u[j][k]/(alpha_est*lambda[k]+1);
			}
		}
	}

	for(i=1; i<=n_cov+1; i++) {
		for (k=1; k<= n_geno_pheno; k++) {
			gtam->d_est[i] = gtam->d_est[i] + m_u[i][k]*d_u[k]/(alpha_est*lambda[k]+1);
		}
	}

    for (k=1; k<= n_geno_pheno; k++) {
        gtam->dd_est = gtam->dd_est + d_u[k]*d_u[k]/(alpha_est*lambda[k]+1);
    }

	free_dmatrix(u,1,n_geno_pheno,1,n_geno_pheno);
	free_dvector(lambda,1,n_geno_pheno);
	free_dmatrix(phi,1,n_geno_pheno,1,n_geno_pheno);
	free_dmatrix(m,1,n_geno_pheno,1,n_cov+1);
	free_dvector(trait,1,n_geno_pheno);
	free_dmatrix(m_u,1,n_cov+1,1,n_geno_pheno);
	free_dvector(d_u,1,n_geno_pheno);

	gtam->n_typed_all = gtam->n_typed_all + n_geno_pheno;

}

void calc_gtam(struct GTAM *gtam, struct DATA_STRUCT data_struct, struct MLE_RESULTS mle_res) {

	int i, j, k;
	int n_cov = data_struct.n_cov;
	double add_var_est = mle_res.add_var_hat;
	double err_var_est = mle_res.err_var_hat;

	double **inv_est, **inv_true;
	inv_est = dmatrix(1,1+n_cov,1,1+n_cov);
	inv_true = dmatrix(1,1+n_cov,1,1+n_cov);
	double det_est, det_true;

	if(n_cov == 1) {
		det_est = gtam->m_est[1][1]*gtam->m_est[2][2]-gtam->m_est[1][2]*gtam->m_est[2][1];

		inv_est[1][1] = gtam->m_est[2][2]/det_est;
		inv_est[2][2] = gtam->m_est[1][1]/det_est;
		inv_est[1][2] = -gtam->m_est[2][1]/det_est;
		inv_est[2][1] = -gtam->m_est[1][2]/det_est;

	} else {
		double **chol, **aug, **cholaug;
		chol = dmatrix(1,1+n_cov,1,1+n_cov);
		aug = dmatrix(1,1+n_cov,1,1+n_cov);
		cholaug = dmatrix(1,1+n_cov,1,1+n_cov);

		for (i=1; i<=1+n_cov; i++) {
			for (j=1; j<=1+n_cov; j++) {
				chol[i][j] = 0.0;
				aug[i][j] = 0.0;
				cholaug[i][j] = 0.0;
				inv_est[i][j] = 0.0;
			}
			aug[i][i] = 1.0;
		}

        int posdef=1;
        posdef = cholesky(gtam->m_est, 1+n_cov, aug, 1+n_cov, chol, cholaug, 1);
        if(posdef == 0) {
            printf("\nERROR: Exiting program!\n"
                   "The matrix M^T Omega^-1 M is not positive semi-definite\n"
                   "(M is the matrix of covariates and genotypes)\n"
                   "Likely this is due to a covarite having the same value for everyone\n"
                   "used in the estimation of variance component and fixed effects\n");
            exit(1);
        }


 		for (i=1; i<=1+n_cov; i++) {
			for (j=1; j<=1+n_cov; j++) {
				for (k=1; k<=1+n_cov; k++) {
					inv_est[i][j] += cholaug[k][i]*cholaug[k][j];
				}
			}
		}


		free_dmatrix(chol,1,1+n_cov,1,1+n_cov);
		free_dmatrix(aug,1,1+n_cov,1,1+n_cov);
		free_dmatrix(cholaug,1,1+n_cov,1,1+n_cov);
	}


	double *eta_est, *eta_true;
	eta_est = dvector(1,1+n_cov);
	eta_true = dvector(1,1+n_cov);

	for (i=1; i<=n_cov+1; i++) {
		eta_est[i] = 0.0;
		eta_true[i] = 0.0;
		for (j=1; j<=n_cov+1; j++) {
			eta_est[i]  = eta_est[i]  + inv_est[i][j]*gtam->d_est[j];
		}
	}

/* changes to correct GTAM the easiest way (see my handwritten notes) */
/* Oct 16, 2015 I (Johanna) need to fix this crappy function */

	double xi_est, xi_true;
	xi_est = gtam->dd_est;
	for (i=1; i<=n_cov+1; i++) {
		xi_est = xi_est - eta_est[i]*gtam->d_est[i];
	}
	xi_est = xi_est/gtam->n_typed_all;

	gtam->t_gtam_est  =  eta_est[n_cov+1]/sqrt((xi_est*inv_est[n_cov+1][n_cov+1]));

	int df;
	double t2_est, t2_true;
	t2_est  = gtam->t_gtam_est*gtam->t_gtam_est;

	df = gtam->n_typed_all-(n_cov-1)-2; /* n_cov. = no covariates including intercept */
	double t = (double) gtam->t_gtam_est;
	gtam->p_value_est = 2*t_q2p(df,t);

	free_dmatrix(inv_est,1,1+n_cov,1,1+n_cov);
	free_dmatrix(inv_true,1,1+n_cov,1,1+n_cov);
	free_dvector(eta_est,1,1+n_cov);
	free_dvector(eta_true,1,1+n_cov);
}
