#include "mastor.h"

#include "nrutil.h"
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <getopt.h>
#include <zlib.h>

#include "brak.h"
#include "brent.h"
#include "calc.h"
#include "cholesky.h"
#include "mle.h"
#include "svdcomp.h"
#include "read.h"
#include "gtam.h"
#include "hashes.h"
#include "datacheck.h"
#include "chisq.h"
#include "tdist.h"
#include "tpr.h"
#include "meta.h"
#include "plot.h"

struct OPT_STRUCT f_opt_struct;

void init_opt_struct(struct FAMILY *family, struct FAMILY_SVD *svd, struct DATA_STRUCT data_struct) {
  f_opt_struct.family = family;
  f_opt_struct.svd = svd;
  f_opt_struct.data_struct = data_struct;
}

int main (int argc, char **argv) {

  printf("\n"
         "  |--------------------------------------------------------|\n"
         "  |                        MASTOR                          |\n"
         "  |          Mixed-Model QTL Association Mapping           |\n"
         "  |                in Related Individuals                  |\n"
         "  |                                                        |\n"
         "  |            Version 0.7.3 - Jan  16, 2017               |\n"
         "  |                                                        |\n"
         "  |                Copyright(C) 2012-2017                  |\n"
         "  |       Johanna Jakobsdottir and Mary Sara McPeek        |\n"
         "  |                 License: GNU GPL v3                    |\n"
         "  |                                                        |\n"
         "  |   http://galton.uchicago.edu/∼mcpeek/software/MASTOR   |\n"
         "  |--------------------------------------------------------|\n\n"
         );

  struct DATA_STRUCT data_struct;
  struct FAMILY *family;
  struct MARKER_STATS stat_marker;
  struct RESULTS results;
  struct FAMILY_SVD *svd;
  struct MLE_RESULTS mle_res;
  struct MLE_PARAM mle_initial;
  struct ANNOTATION anno;

  int i,j,k;
  double alpha;
  int n_pheno;
  int arg;

  double like_from, like_to, like_by;

  struct HASH hash;
  /*hash = (struct HASH) malloc(sizeof(struct HASH));*/
  hash.fam2fam = kh_init(str);
  hash.ind2fam = kh_init(str);
  hash.ind2ind = kh_init(str);

  char *pedfile, *genofile, *kinfile;
  pedfile = (char*) malloc(MAXLEN);
  genofile = (char*) malloc(MAXLEN);
  kinfile = (char*) malloc(MAXLEN);

  char *prefix;
  prefix = (char*) malloc(MAXLEN);
  strcpy(prefix,"");
  char *association_outfile;
  association_outfile = (char*) malloc(MAXLEN);
  strcpy(association_outfile, "");
  char *mle_outfile;
  mle_outfile = (char*) malloc(MAXLEN);
  strcpy(mle_outfile, "");

  strcpy(pedfile,"ped.txt");
  strcpy(genofile,"geno.txt");
  strcpy(kinfile,"kin.txt");

  int pfile=0, gfile=0, kfile=0;
  int likeplot = 0;
  int nullmle = 0;
  int option = 0;
  int subset = -9;
  int tpr = 0;
  int meta = 0;

  const char* opt_string = "p:g:k:a:m:s:";
  static struct option long_options[] =
  {
    {"pedfile",required_argument,0,'p'},
    {"genofile", required_argument,0,'g'},
    {"kinfile",required_argument,0,'k'},
    {"analysis",required_argument,0,'a'},
    {"mixedMLE",no_argument,0,'m'},
    {"like",no_argument,0,0},
    {"from",required_argument,0,0},
    {"to",required_argument,0,0},
    {"by",required_argument,0,0},
    {"subset",required_argument,0,'s'},
    {"prefix",required_argument,0,0},
    {"tpr",no_argument,0,0},
    {"meta",no_argument,0,0},
    {0,0,0,0}
  };

  int option_index = 0, c;
  while (1) {

    c = getopt_long(argc,argv,opt_string,long_options, &option_index);
    if(c == -1) {
      break;
    }

    switch(c) {
      case 'p':
        strcpy(pedfile, optarg);
        printf("User specified pedigree and phenotype file: %s\n", pedfile);fflush(stdout);
        pfile = 1;
        break;
      case 'g':
        strcpy(genofile, optarg);
        printf("User specified genotype file: %s\n", genofile);fflush(stdout);
        gfile = 1;
        break;
      case 'k':
        strcpy(kinfile, optarg);
        printf("User specified kinship information file: %s\n", kinfile);fflush(stdout);
        kfile = 1;
        break;
      case 'a':
        if(strcmp("mastor",optarg) == 0) {
          printf("\nUser requested MASTOR analysis only\n");fflush(stdout);
          option = 1;
        }
        if(strcmp("gtam",optarg) == 0) {
          printf("\nUser requested GTAM analysis only\n");fflush(stdout);
          option = 2;
        }
        break;
      case 'm':
        printf("\nUser requested fitting null model only\n");fflush(stdout);
        nullmle = 0;
        break;
      case 's':
        subset = atof(optarg);
        break;
      case 0:
        if (strcmp("like",long_options[option_index].name) == 0) {
          printf("\nRange of likelihood values will be calculated and written out to file\n");fflush(stdout);
          likeplot = 1;
          nullmle = 1;
        }
        if (strcmp("from",long_options[option_index].name) == 0) {
          like_from = atof(optarg);
        }
        if (strcmp("to",long_options[option_index].name) == 0) {
          like_to = atof(optarg);
        }
        if (strcmp("by",long_options[option_index].name) == 0) {
          like_by = atof(optarg);
        }
        if (strcmp("prefix",long_options[option_index].name) == 0) {
          strcpy(prefix,optarg);
        }
        if (strcmp("tpr",long_options[option_index].name) == 0) {
          printf("\nUser requested TPR to be calculated\n");fflush(stdout);
          tpr = 1;
          subset = 0;
          option = 1;
          nullmle = 0;
        }
        if (strcmp("meta",long_options[option_index].name) == 0) {
          printf("\nUser requested summary statistics for meta-analysis\n");fflush(stdout);
          meta = 1;
        }
        break;
      default:
        exit(1);
    } // close switch
  } // close while

  if(option == 0 & tpr == 0) {
    printf("\nUser did not specify analysis, performing MASTOR analysis only\n");fflush(stdout);
    option = 1;
  }

  if (likeplot==1) {
    printf("\tThe range is between %lf and %lf with increment %lf\n",like_from,like_to,like_by);
  }

  if(subset == -9) {
    //printf("\nUser did not specify subset to use in VC estimation. Using defaults\n");fflush(stdout);
    if(option == 1) {subset = 3;} // option = 1 = MASTOR
    if(option == 2) {subset = 1;} // option = 2 = GTAM
  }

  data_struct.tol = NUMTOL;
  data_struct.missing_val = MISSVAL;

  format_checks(pedfile,genofile,kinfile,&data_struct);
  printf("\nWARNING: User should make sure these counts are as expected. Please see the documentation for details\n");
  printf("n_fam = %d\nn_marker = %d\nn_total = %d\nn_typed = %d\nn_cov = %d\n",data_struct.n_fam,data_struct.n_marker,data_struct.n_total,data_struct.n_typed,data_struct.n_cov);

  int n_fam = data_struct.n_fam;
  int n_total = data_struct.n_total;
  int n_typed = data_struct.n_typed;

  char **id_geno2all = (char**)malloc((n_typed+1) * sizeof(char*)); /* id_geno2all[unique index id over all genotyped ind across all ped] = unique index id over all ind across all ped*/

  int n_cov = data_struct.n_cov;
  int unr;

  FILE *out_param;
  FILE *outfile;
  gzFile markfile;

  anno.chr = (char*) malloc(4);
  anno.markername = (char*) malloc(MAXLEN);

  /* ====================================================================================== */
  /* this part is for MASTOR analysis only (not GTAM analysis)                              */
  /* ====================================================================================== */

    /* option 1 = run MASTOR */
    if(option == 1) {

      printf("\nStarting MASTOR analysis:\n");

      family = (struct FAMILY *)malloc(sizeof(struct FAMILY)*(n_fam+1));
      svd = (struct FAMILY_SVD *)malloc(sizeof(struct FAMILY_SVD)*(n_fam+1));

      readped(pedfile,family, &hash, data_struct);
      readkin(kinfile,family, hash);
      markfile = gzopen(genofile,"r");
      marker_skrats_allocate(family, data_struct);

      if(markfile == NULL) {
        printf("Can't open file with marker genotypes\n");
        exit(1);
      }

      /* void readmarker_header(FILE *markfile, char **id_geno2all, struct FAMILY *family, struct HASH hash, struct DATA_STRUCT data_struct); */
      readmarker_header(markfile, id_geno2all, family, hash, data_struct);

      /* void fill_pheno_subset(struct FAMILY *family, struct DATA_STRUCT data_struct); */
      fill_pheno_subset(family, data_struct);

      /* int unrelated_check (struct FAMILY *family, struct DATA_STRUCT data_struct, int subset, int relpair); */
      unr = unrelated_check(family, data_struct, subset, 1);

      mle_initial.tol = NUMTOL;
      initial_ax(family, &mle_initial, data_struct);

      for(i=1; i<= n_fam;i++) {
        /* void fill_struct_svd(struct FAMILY family, struct DATA_STRUCT data_struct, struct FAMILY_SVD *svd, int subset); */
        fill_struct_svd(family[i], data_struct, &svd[i],subset);
      }
      init_opt_struct(family, svd, data_struct);

      /* if sample contains enough phenotyped relative pairs */
      if(unr==0) {
        initial_brak(&mle_initial, &f_neg_log_like_mle);

        alpha = estimate_alpha_hat(mle_initial);
        mle_res.alpha_hat = alpha;
        mle_res.var_beta_hat = dvector(1,data_struct.n_cov);
        mle_res.beta_hat = dvector(1,data_struct.n_cov);
        mle_res.beta_ols = dvector(1,data_struct.n_cov);

        if(likeplot==1) {
          char *filename;
          filename = (char*) malloc(MAXLEN);
          strcpy(filename,prefix);

          if(strcmp("",filename) == 0) {
              strcat(filename,"like_plot_MASTOR.txt");
          } else {
              strcat(filename,"_like_plot_MASTOR.txt");
          }

          plot_like(filename,like_from,like_to,like_by,&f_neg_log_like_mle);

          printf("Likelihood values are in file %s:\n", filename);

          free(filename);

        } // end if(likeplot==1)

        /* if additive variance < 0 try to include more individuals in the estimation */
        if (alpha < 0.0) {
          for(i=1; i<= n_fam;i++) {
            /* void fill_struct_svd(struct FAMILY family, struct DATA_STRUCT data_struct, struct FAMILY_SVD *svd, int subset);
             subset = 0 includes all phenotyped individuals in the estimation */
            fill_struct_svd(family[i], data_struct, &svd[i],0);
          }
          init_opt_struct(family, svd, data_struct);
          initial_brak(&mle_initial, &f_neg_log_like_mle);

          alpha = estimate_alpha_hat(mle_initial);
          mle_res.alpha_hat = alpha;
          mle_res.var_beta_hat = dvector(1,data_struct.n_cov);
          mle_res.beta_hat = dvector(1,data_struct.n_cov);
          mle_res.beta_ols = dvector(1,data_struct.n_cov);

          if(likeplot==1) {
            char *filename;
            filename = (char*) malloc(MAXLEN);
            strcpy(filename,prefix);

            if(strcmp("",filename) == 0) {
              strcat(filename,"like_plot_including_all_inds.txt");
            } else {
              strcat(filename,"_like_plot_including_all_inds.txt");
            }

            plot_like(filename,like_from,like_to,like_by,&f_neg_log_like_mle);

            printf("Likelihood values are in file %s:\n", filename);

            free(filename);
          }

        } /* done including more individuals */

        /* if additive variance is still < 0 after including additional individuals in the estimation.
            Then use OLS to replace the GLS. Use the original smaller subset
                else proceed with using the GLS and calcualte the OLS but not replace GLS with OLS */
        /* void estimate_mle_ols (struct MLE_RESULTS *mle_res, struct FAMILY *family, struct DATA_STRUCT data_struct, int subset, int alpha_neg); */

        if (alpha < 0.0) {
            mle_res.alpha_hat = 0.0;
            estimate_mle_ols (&mle_res, family, data_struct, subset, 1);
        } else {
          estimate_mle (&mle_res, svd, data_struct);
          estimate_mle_ols (&mle_res, family, data_struct, subset, 0);
        }
      }


      /* if sample does not contain enough (or none) phenotyped relative pairs */
      if(unr==1) {
        mle_res.alpha_hat = 0.0;
        mle_res.var_beta_hat = dvector(1,data_struct.n_cov);
        mle_res.beta_hat = dvector(1,data_struct.n_cov);
        mle_res.beta_ols = dvector(1,data_struct.n_cov);
        estimate_mle_ols (&mle_res, family, data_struct, 2, 1);

        printf("WARNING: Not enough relative pairs for VC estimation, using OLS estimators for betas\n");

        if(likeplot==1) {
          printf("WARNING: User selected option to calculate various values of likelihood\n"
                 "Calculations were not performed because sample did not contain any\n"
                 "relative pairs to estimate the additive variance, thus only the error\n"
                 "variance is estimated using ordinary least squares\n");
        }
      } // end if(unr==1)

      strcpy(association_outfile,prefix);
      strcpy(mle_outfile,prefix);

      if(strcmp("",prefix) == 0) {
        strcat(association_outfile,"MASTOR.txt");
        strcat(mle_outfile,"MASTOR_MLEs.txt");
      } else {
        char postfix[20];
        strcpy(postfix,"_MASTOR.txt");
        strcat(association_outfile,postfix);
        strcpy(postfix,"_MASTOR_MLEs.txt");
        strcat(mle_outfile,postfix);
      }

      /* output TPR (a_vector) = transformed phenotypic residuals */
      if(tpr == 1) {
        char *tpr_outfile;
        tpr_outfile = (char*) malloc(MAXLEN);
        strcpy(tpr_outfile, "");
        strcpy(tpr_outfile,prefix);
        if(strcmp("",prefix) == 0) {
          strcat(tpr_outfile,"TPR.txt");
        } else {
          char postfix[20];
          strcpy(postfix,"_TPR.txt");
          strcat(tpr_outfile,postfix);
        }
        for(i=1; i<= n_fam;i++) {
          /* void a_vector(struct FAMILY *family, struct DATA_STRUCT data_struct, struct MLE_RESULTS mle_res, int subset); */
          a_vector(&family[i], data_struct, mle_res,subset);
        }
        print_tpr(tpr_outfile,family, hash, data_struct);
        free(tpr_outfile);
        printf("TPR calculation done:\n"
               "\tTPR values are in file: %s\n", tpr_outfile);
      } // end if(tpr==1)

      FILE* meta_outfile = 0;
      if(meta == 1) {
        char *meta_outfilename;
        meta_outfilename = (char*) malloc(MAXLEN);
        strcpy(meta_outfilename, "");
        strcpy(meta_outfilename,prefix);
        if(strcmp("",prefix) == 0) {
          strcat(meta_outfilename,"meta.txt");
        } else {
          char postfix[20];
          strcpy(postfix,"_meta.txt");
          strcat(meta_outfilename,postfix);
        }
        meta_outfile = fopen(meta_outfilename,"w");
        fprintf(meta_outfile,"Chr\tMarker\tbp\tv1\tv2\tv3\tv4\tv5\tv6\tn\n");
      } // end if(meta==1)

      /* perform association analysis only if requested, option =  1 (MASTOR) */
//        if (nullmle != 1) {
      if (nullmle != 1 & tpr !=1) {
      outfile = fopen(association_outfile,"w");

      fprintf(outfile,"Chr\tMarker\tbp\tMASTOR\tMASTOR-P\tASTOR\tASTOR-P\tFreq\n");

      /* for MASTOR (previously called mqlsq, which is sometimes in the code) */
      for(i=1; i<= n_fam;i++) {
        /* void a_vector(struct FAMILY *family, struct DATA_STRUCT data_struct, struct MLE_RESULTS mle_res, int subset); */
        a_vector(&family[i], data_struct, mle_res,subset);
        //a_vector_true(&family[i], data_struct);
        a_vector_i(&family[i], data_struct, mle_res,subset);
      }

      /* cycle through all markers and calculate each statistic */
      for(j=1; j<= data_struct.n_marker;j++) {
        marker_skrats_initialize(family, data_struct);
        readmarker(markfile, family, hash, id_geno2all, data_struct, &anno);

        /* running with estimated omega */

        stat_marker.v_1 = 0.0;
        stat_marker.v_2 = 0.0;
        stat_marker.v_3 = 0.0;
        stat_marker.v_4 = 0.0;
        stat_marker.v_5 = 0.0;
        stat_marker.v_6 = 0.0;
        stat_marker.n = 0;

        for(i=1; i<= data_struct.n_fam;i++) {
          family[i].a_all = family[i].a_store[1];
          //aug_phi_nn(j, i, family[i], &stat_marker);
          svd_phi_nn(j, i, family[i], &stat_marker);
        }
        calculations(j, stat_marker, &results);
        fprintf(outfile,"%s\t%s\t%d\t%lf\t%g\t",anno.chr,anno.markername,anno.basepair,results.mqlsq1,results.p_value1);
        if(meta==1) {
          print_meta(meta_outfile, j, anno, stat_marker);
        }

        /* running with omega = I */

        stat_marker.v_1 = 0.0;
        stat_marker.v_2 = 0.0;
        stat_marker.v_3 = 0.0;
        stat_marker.v_4 = 0.0;
        stat_marker.v_5 = 0.0;
        stat_marker.v_6 = 0.0;
        stat_marker.n = 0;

        for(i=1; i<= data_struct.n_fam;i++) {
          family[i].a_all = family[i].a_store[3];
          //aug_phi_nn(j, i, family[i], &stat_marker);
          svd_phi_nn(j, i, family[i], &stat_marker);
        }
        calculations(j, stat_marker, &results);
        fprintf(outfile,"%lf\t%g\t%lf\n",results.mqlsq1,results.p_value1,results.p_0_blue);

        if(j % ANALYSIS_STATUS == 0 ) {
          printf("\tMarkers done %d\n",j);
        }

      } /* end for loop to cycle through all markers and calculate each statistic */

      fclose(outfile);
      if(meta==1) {
        fclose(meta_outfile);
      }
    }

    gzclose(markfile);
    out_param = fopen(mle_outfile, "w");
    fprintf(out_param,"Parameter\tnullMLE\tSE_nullMLE\tnullMLE_OLS\n");
    fprintf(out_param,"Heritability\t%lf\t%lf\tNA\n",mle_res.herit,sqrt(mle_res.var_herit));
    fprintf(out_param,"Additive_Var\t%lf\t%lf\tNA\n",mle_res.add_var_hat,sqrt(mle_res.var_add_var_hat));
    fprintf(out_param,"Error_Var\t%lf\t%lf\tNA\n",mle_res.err_var_hat,sqrt(mle_res.var_err_var_hat));
    fprintf(out_param,"Intercept\t%lf\t%lf\t%lf\n",mle_res.beta_hat[1],sqrt(mle_res.var_beta_hat[1]),mle_res.beta_ols[1]);
    for(j=2; j<= data_struct.n_cov;j++) {
      fprintf(out_param,"Covariate_%d\t%lf\t%lf\t%lf\n",j-1,mle_res.beta_hat[j],sqrt(mle_res.var_beta_hat[j]),mle_res.beta_ols[j]);
    }
    fclose (out_param);

    marker_skrats_free(family, data_struct);

    for(i=1; i<= n_fam;i++) {
      n_pheno = family[i].n_pheno;
      if (svd[i].n_ind != 0) {
        free_dvector(svd[i].trait_u,1,n_pheno);
        free_dmatrix(svd[i].cov_u,1,n_pheno,1,n_cov);
        free_dvector(svd[i].lambda,1,n_pheno);
      }

      free_dmatrix(family[i].phi,1,n_total,1,n_total);
      free_ivector(family[i].geno_typed,1,n_total);
      free_ivector(family[i].pheno_typed,1,n_pheno);
      free_ivector(family[i].pheno_typed_inv,1,n_total);
      free_dvector(family[i].trait,1,n_pheno);
      free_dmatrix(family[i].cov,1,n_pheno,1,n_cov);
      free_dmatrix(family[i].a_store,1,3,1,n_pheno);

    }

    free_dvector(mle_res.var_beta_hat,1,data_struct.n_cov);
    free_dvector(mle_res.beta_hat,1,data_struct.n_cov);
    if(unr==0){ free_dvector(mle_res.beta_ols,1,data_struct.n_cov); }

    free(family);
    free(svd);

    if(nullmle == 1) {
      printf("MLE estimation done:\n"
             "\tParameter estimates are in file: %s\n", mle_outfile);
    }
    if (nullmle != 1 & tpr !=1) {
      printf("MASTOR analysis done:\n"
             "\tAssociation results are in file: %s\n"
             "\tParameter estimates are in file: %s\n",association_outfile,mle_outfile);
    }
  }


  /* ====================================================================================== */
  /* this part is for GTAM analysis only (not MASTOR analysis)                              */
  /* ====================================================================================== */


  /* option 2 run GTAM */
  if(option == 2) {

    printf("\nStarting GTAM analysis:\n");

    family = (struct FAMILY *)malloc(sizeof(struct FAMILY)*(n_fam+1));
    svd = (struct FAMILY_SVD *)malloc(sizeof(struct FAMILY_SVD)*(n_fam+1));

    readped(pedfile,family,&hash,data_struct);
    readkin(kinfile,family,hash);
    markfile = gzopen(genofile, "r");
    marker_skrats_allocate(family, data_struct);

    if(markfile == NULL) {
      printf("Can't open file with marker genotypes\n");
      exit(1);
    }

    readmarker_header(markfile, id_geno2all, family, hash, data_struct);
    fill_pheno_subset(family, data_struct);

    /* int unrelated_check (struct FAMILY *family, struct DATA_STRUCT data_struct, int subset, int relpair); */
    unr = unrelated_check(family, data_struct, subset, 1);

    mle_initial.tol = NUMTOL;
    initial_ax(family, &mle_initial, data_struct);

    /* void fill_struct_svd(struct FAMILY family, struct DATA_STRUCT data_struct, struct FAMILY_SVD *svd, int subset); */
    for(i=1; i<= n_fam;i++) {
      fill_struct_svd(family[i], data_struct, &svd[i],subset);
    }
    init_opt_struct(family, svd, data_struct);

    /* if sample contains enough phenotyped relative pairs */
    if(unr==0) {
      initial_brak(&mle_initial, &f_neg_log_like_mle);

      alpha = estimate_alpha_hat(mle_initial);
      mle_res.alpha_hat = alpha;
      mle_res.var_beta_hat = dvector(1,data_struct.n_cov);
      mle_res.beta_hat = dvector(1,data_struct.n_cov);
      mle_res.beta_ols = dvector(1,data_struct.n_cov);

      if(likeplot==1) {
        char *filename;
        filename = (char*) malloc(MAXLEN);
        strcpy(filename,prefix);

        if(strcmp("",filename) == 0) {
          strcat(filename,"like_plot_GTAM.txt");
        } else {
          strcat(filename,"_like_plot_GTAM.txt");
        }

        plot_like(filename,like_from,like_to,like_by,&f_neg_log_like_mle);

        printf("Likelihood values are in file %s:\n", filename);

        free(filename);
      } // end if(likeplot==1)

      /* void estimate_mle_ols (struct MLE_RESULTS *mle_res, struct FAMILY *family, struct DATA_STRUCT data_struct, int subset, int alpha_neg); */

      if (alpha < 0.0) {
        mle_res.alpha_hat = 0.0;
        estimate_mle_ols (&mle_res, family, data_struct, subset, 1);
      } else {
        estimate_mle (&mle_res, svd, data_struct);
        estimate_mle_ols (&mle_res, family, data_struct, subset, 0);
      }
    } // end if(unr==0)

    /* if sample does not contain enough (or none) phenotyped relative pairs */
    if(unr==1) {
      mle_res.alpha_hat = 0.0;
      mle_res.var_beta_hat = dvector(1,data_struct.n_cov);
      mle_res.beta_hat = dvector(1,data_struct.n_cov);
      mle_res.beta_ols = dvector(1,data_struct.n_cov);
      estimate_mle_ols (&mle_res, family, data_struct, subset, 1);

      printf("WARNING: Not enough relative pairs for VC estimation, using OLS estimators for betas\n");

    }



    /* for gtam */
    struct GTAM gtam;
    /*gtam.m_true = dmatrix(1,n_cov+1,1,n_cov+1);*/
    gtam.m_est = dmatrix(1,n_cov+1,1,n_cov+1);
    /*gtam.d_true = dvector(1,n_cov+1);*/
    gtam.d_est = dvector(1,n_cov+1);

    strcpy(association_outfile,prefix);
    strcpy(mle_outfile,prefix);

    if(strcmp("",prefix) == 0) {
      strcat(association_outfile,"GTAM.txt");
      strcat(mle_outfile,"GTAM_MLEs.txt");
    } else {
      char postfix[20];
      strcpy(postfix,"_GTAM.txt");
      strcat(association_outfile,postfix);
      strcpy(postfix,"_GTAM_MLEs.txt");
      strcat(mle_outfile,postfix);
    }


    /* perform association analysis only if requested, option =  2 (GTAM) */
    if (nullmle != 1) {

      outfile = fopen(association_outfile,"w");

      fprintf(outfile,"Chr\tMarker\tbp\tGTAM\tGTAM-P\tn\n");

      /* cycle through all markers and calculate each statistic */
      for(j=1; j<= data_struct.n_marker;j++) {
        marker_skrats_initialize(family, data_struct);
        readmarker(markfile, family, hash, id_geno2all, data_struct, &anno);

        /* run gtam */
        initialize_gtam(&gtam, data_struct);
        gtam.n_typed_all = 0;
        for(i=1; i<=data_struct.n_fam; i++) {
          fill_gtam(&gtam, family[i], data_struct, mle_res);
        }
        calc_gtam(&gtam, data_struct, mle_res);
        fprintf(outfile,"%s\t%s\t%d\t%lf\t%g\t%d\n",anno.chr,anno.markername,anno.basepair,gtam.t_gtam_est,gtam.p_value_est,gtam.n_typed_all);

        if(j % ANALYSIS_STATUS == 0 ) {
          printf("\tMarkers done %d\n",j);
        }

      } /* end for loop to cycle through all markers and calculate each statistic */

      fclose(outfile);
    }

    gzclose(markfile);

    out_param = fopen(mle_outfile,"w");
    fprintf(out_param,"Parameter\tnullMLE\tSE_nullMLE\tnullMLE_OLS\n");
    fprintf(out_param,"Heritability\t%lf\t%lf\tNA\n",mle_res.herit,sqrt(mle_res.var_herit));
    fprintf(out_param,"Additive_Var\t%lf\t%lf\tNA\n",mle_res.add_var_hat,sqrt(mle_res.var_add_var_hat));
    fprintf(out_param,"Error_Var\t%lf\t%lf\tNA\n",mle_res.err_var_hat,sqrt(mle_res.var_err_var_hat));
    fprintf(out_param,"Intercept\t%lf\t%lf\t%lf\n",mle_res.beta_hat[1],sqrt(mle_res.var_beta_hat[1]),mle_res.beta_ols[1]);
    for(j=2; j<= data_struct.n_cov;j++) {
      fprintf(out_param,"Covariate_%d\t%lf\t%lf\t%lf\n",j-1,mle_res.beta_hat[j],sqrt(mle_res.var_beta_hat[j]),mle_res.beta_ols[j]);
    }
    fclose (out_param);


    marker_skrats_free(family, data_struct);

    for(i=1; i<= n_fam;i++) {
      n_pheno = family[i].n_pheno;
      if (svd[i].n_ind != 0) {
        free_dvector(svd[i].trait_u,1,n_pheno);
        free_dmatrix(svd[i].cov_u,1,n_pheno,1,n_cov);
        free_dvector(svd[i].lambda,1,n_pheno);
      }

      free_dmatrix(family[i].phi,1,n_total,1,n_total);
      free_ivector(family[i].geno_typed,1,n_total);
      free_ivector(family[i].pheno_typed,1,n_pheno);
      free_ivector(family[i].pheno_typed_inv,1,n_total);
      free_dvector(family[i].trait,1,n_pheno);
      free_dmatrix(family[i].cov,1,n_pheno,1,n_cov);
      free_dmatrix(family[i].a_store,1,3,1,n_pheno);
    }

    free_dvector(mle_res.var_beta_hat,1,data_struct.n_cov);
    free_dvector(mle_res.beta_hat,1,data_struct.n_cov);
    if(unr==0){ free_dvector(mle_res.beta_ols,1,data_struct.n_cov); }

    /*free_dmatrix(gtam.m_true,1,n_cov+1,1,n_cov+1);*/
    free_dmatrix(gtam.m_est,1,n_cov+1,1,n_cov+1);
    /*free_dvector(gtam.d_true,1,n_cov+1);*/
    free_dvector(gtam.d_est,1,n_cov+1);

    /*free_dvector(data_struct.beta,1,n_cov);*/
    free(family);
    free(svd);

    if(nullmle == 1) {
      printf("GTAM analysis done:\n"
             "\tParameter estimates are in file: %s\n",mle_outfile);
    } else {
      printf("GTAM analysis done:\n"
             "\tAssociation results are in file: %s\n"
             "\tParameter estimates are in file: %s\n",association_outfile,mle_outfile);
    }

  }

  free(pedfile);
  free(genofile);
  free(kinfile);

  hash.fam2fam = kh_init(str);
  hash.ind2fam = kh_init(str);
  hash.ind2ind = kh_init(str);

  /* free all keys and the hashtable itself */
  for (k = kh_begin(hash.fam2fam); k != kh_end(hash.fam2fam); k++){
    if (kh_exist(hash.fam2fam,k)) {
      free((char*) kh_key(hash.fam2fam,k)); /* cast away constness */
    }
  }
  kh_destroy(str,hash.fam2fam);

  for (k = kh_begin(hash.ind2fam); k != kh_end(hash.ind2fam); k++){
    if (kh_exist(hash.ind2fam,k)) {
      free((char*) kh_key(hash.ind2fam,k)); /* cast away constness */
    }
  }
  kh_destroy(str,hash.ind2fam);

  for (k = kh_begin(hash.ind2ind); k != kh_end(hash.ind2ind); k++){
    if (kh_exist(hash.ind2ind,k)) {
      free((char*) kh_key(hash.ind2ind,k)); /* cast away constness */
    }
  }
  kh_destroy(str,hash.ind2ind);


  return 0;
}
