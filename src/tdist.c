#include <math.h>
#include <assert.h>
#include "tdist.h"

/* assume b = 1/2, a = df/2, where df = df of T distribution
    Returns one-sided test */
double t_q2p(const int df, const double t)
{
    double a = df/2.0;
    double x = df/(df+t*t);
    return 0.5*beta_inc(a,x);
}

double beta_inc(const double a,const double x)
{
    const double b = 0.5;
    double val;

    if(x == 0.0) {
        return  0.0;
    } else if(x == 1.0) {
        return 1.0;
    } else {
        const double sqrt_pi = 1.77245385090551;
        //double ln_beta = log(sqrt_pi)-0.5*log(a);
        double ln_beta = log(sqrt_pi)+lngamma_lanczos(a)-lngamma_lanczos(a+0.5);
        double ln_1mx = log1p(-x);
        double ln_x = log1p(x-1);

        const double ln_pre_val = -ln_beta + a * ln_x + b * ln_1mx;
        const double prefactor = exp(ln_pre_val);

        if(x < (a + 1.0)/(a+b+2.0)) {
            /* Apply continued fraction directly. */
            double cf = beta_cont_frac(a, b, x);
            val = prefactor * cf / a;
        } else {
            /* Apply continued fraction after hypergeometric transformation. */
            double cf = beta_cont_frac(b, a, 1.0-x);
            const double term = prefactor * cf / 0.5;
            val  = 1.0 - term;
        }
    }
    return val;
}

double beta_cont_frac(const double a, const double b, const double x) {
    const unsigned int max_iter = 512;
    const double cutoff = 2.0*2.2250738585072014e-308;
    const double eps = 2.2204460492503131e-16;
    unsigned int iter_count = 0;
    double cf;

    /* standard initialization for continued fraction */
    double num_term = 1.0;
    double den_term = 1.0 - (a+b)*x/(a+1.0);
    if (fabs(den_term) < cutoff) den_term = cutoff;
    den_term = 1.0/den_term;
    cf = den_term;

    while(iter_count < max_iter) {
          const int k  = iter_count + 1;
          double coeff = k*(b-k)*x/(((a-1.0)+2*k)*(a+2*k));
          double delta_frac;

          /* first step */
          den_term = 1.0 + coeff*den_term;
          num_term = 1.0 + coeff/num_term;
          if(fabs(den_term) < cutoff) den_term = cutoff;
          if(fabs(num_term) < cutoff) num_term = cutoff;
          den_term  = 1.0/den_term;

          delta_frac = den_term * num_term;
          cf *= delta_frac;

          coeff = -(a+k)*(a+b+k)*x/((a+2*k)*(a+2*k+1.0));

          /* second step */
          den_term = 1.0 + coeff*den_term;
          num_term = 1.0 + coeff/num_term;
          if(fabs(den_term) < cutoff) den_term = cutoff;
          if(fabs(num_term) < cutoff) num_term = cutoff;
          den_term = 1.0/den_term;

          delta_frac = den_term*num_term;
          cf *= delta_frac;

          if(fabs(delta_frac-1.0) < 2.0*eps) break;

          ++iter_count;
    } // close while(iter_count < max_iter)

    if(iter_count >= max_iter){
        return -9.0; // error
    } else {
      return cf;
    }
}

/* know x = df/2 or x = df/2+1/2
    Assume df >= 5, i.e. N = df - n_cov - 2 > 3-n_cov
    in gsl_sf_lngamma_e(double x, gsl_sf_result * result)
    skip the Pade evaluations for x close to 1 or 2 (0.01 dist)
    skip x = 0 or close (0.02 dist) from 0
    all above was in gsl_sf_lngamma_e, we do not need it */

double lngamma_lanczos(double x) {
    const double eps = 2.2204460492503131e-16;
    const double LogRootTwoPi = 0.9189385332046727418;

    int k;
    double Ag;
    double term1,term2;
    x -= 1.0; //Lanczos writes z! instead of Gamma(z)
    Ag = lanczos_7_c[0];
    for(k=1; k<=8; k++) {
        Ag += lanczos_7_c[k]/(x+k);
    }
    /* (x+0.5)*log(x+7.5) - (x+7.5) + LogRootTwoPi_ + log(Ag(x)) */
    term1 = (x+0.5)*log((x+7.5)/M_E);
    term2 = LogRootTwoPi + log(Ag);
    return term1 + (term2 - 7.0);
}
