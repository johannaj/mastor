#ifndef PLOT_H
#define PLOT_H

#include "mastor.h"

void plot_like (char *filename, double x1, double x2, double inc, double (*func)(double));

#endif
