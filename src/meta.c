#include "meta.h"

/* file to output summary stats for meta-analysis
   has to be open before calling this function */
void print_meta(FILE *file, int marker, struct ANNOTATION anno, struct MARKER_STATS stat) {

  int n = stat.n;
  double v1 = stat.v_1;
  double v2 = stat.v_2;
  double v3 = stat.v_3;
  double v4 = stat.v_4;
  double v5 = stat.v_5;
  double v6 = stat.v_6;

  fprintf(file,"%s\t%s\t%d\t%lf\t%lf\t%lf\t%lf\t%lf\t%lf\t%d\n",anno.chr,anno.markername,anno.basepair,v1,v2,v3,v4,v5,v6,n);

}
