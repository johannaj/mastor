MASTOR is an association test to test for association of genetic variant (SNP) with (quantitative) phenotype. It uses a retrospective model, which allows MASTOR to take advantage of incomplete data.

### What is this repository for? ###

* This is a developmental repository, which always has the most up-to-date version of MASTOR.
* MASTOR is also available at http://www.stat.uchicago.edu/~mcpeek/software/MASTOR/index.html

### References ###

* The paper describing MASTOR can be found at
http://www.cell.com/ajhg/pdf/S0002-9297(13)00122-5.pdf
* Technical report can be found at
https://notendur.hi.is/johannaj/MASTORtech.pdf

### Funding ###
Funding for the development of MASTOR was provided by

* The National Institutes of Health (R01HG001645 to Mary Sara McPeek).
* The Icelandic Heart Association.
* The Icelandic Research Fund (130726-051, 130726-052, 130726-053 to Johanna Jakobsdottir).

### License ###
The MASTOR program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.

MASTOR includes code provided by others under licenses compatible with GNU GPL as free software:

* CLAPACK, which is under the 3-clause BSD license.
* Numerical recipes utility functions, which are public domain.
* GNU Scientific Library, which is under GNU GPL version 3 (or later).
* Hash table library by Attractive Cahos, which is under an Open Source MIT License.